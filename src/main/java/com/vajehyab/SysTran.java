package com.vajehyab;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

public class SysTran {
    public static void main(String[] args) {
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);

        if (args.length == 0) {
            return;
        }

        try (final WebClient webClient = new WebClient(BrowserVersion.CHROME)) {
            webClient.getOptions().setUseInsecureSSL(true);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.getOptions().setJavaScriptEnabled(false);

            final HtmlPage page = webClient.getPage("https://www.vajehyab.com/?q=" + URLEncoder.encode(args[0], "UTF-8"));
            List sections = page.getByXPath("//*[@id=\"rightside\"]/section[1]/p[3]");

            if (sections.size() >= 1) {
                HtmlParagraph paragraph = (HtmlParagraph) sections.get(0);
                String content = paragraph.getTextContent();
                String phonetics = content.substring(content.indexOf(":") + 1).trim();
                System.out.println(phonetics);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
